<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SubastaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware' => 'auth'], function() {
    Route::get('/',[SubastaController::class,'home']);     
    Route::get('/auctions',[SubastaController::class,'auctions']);     
    Route::get('/bids',[SubastaController::class,'bids']);     
    Route::get('/auction',[SubastaController::class,'auction']);     
    Route::get('/bid/{id}',[SubastaController::class,'puja']);     
    Route::get('/bid/lowerPrice/{idSubasta}',[SubastaController::class,'lowerPrice']);     
    Route::get('/check',[SubastaController::class,'subastaOut']);     
    Route::get('/myObjects',[SubastaController::class,'getVehiculos']); 
    Route::get('/newItem',[SubastaController::class,'setVehiculos'])->name('setCar');
    Route::post('/setItem',[SubastaController::class,'setVehiculos']);
});

//controllerSubasta
//Route::get('/',[controllerSubasta::class,'puja']);

/*Route::get('/auctions',[controllerSubasta::class,'auctions'])->middleware('auth');
Route::get('/bids/{id}',[controllerSubasta::class,'bids'])->middleware('auth');
Route::get('/auction',[controllerSubasta::class,'auction'])->middleware('auth');
Route::get('/bid/idsubasta/{id}',[controllerSubasta::class,'puja'])->middleware('auth');
Route::get('/bid/lowerPrice/idsubasta/{id}',[controllerSubasta::class,'lowerPrice'])->middleware('auth');
Route::get('/check/{id}',[controllerSubasta::class,'subastaOut'])->middleware('auth');
Route::get('/myObjects/{id}',[controllerSubasta::class,'getVehiculos'])->middleware('auth');*/


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('/');
