<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubastas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Subastas', function (Blueprint $table) {
            $table->increments('Id_Subasta');
            $table->integer('Puja_Min');
            $table->dateTime('Fecha_Fin');
            $table->boolean('Activa');
            $table->integer('Id_Usuario')->unsigned()->index();
            $table->integer('Id_Vehiculo')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Subastas');
    }
}
