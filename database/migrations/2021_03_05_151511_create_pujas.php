<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePujas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pujas', function (Blueprint $table) {
            $table->increments('Id_Puja');
            $table->integer('Precio');
            $table->integer('Id_Subasta')->unsigned()->index();
            $table->integer('Id_Usuario')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Pujas');
    }
}
