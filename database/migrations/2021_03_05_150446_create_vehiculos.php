<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Vehiculos', function (Blueprint $table) {
            $table->increments('Id_Vehiculo');
            $table->string('Nom', 100);
            $table->string('Matricula', 20);
            $table->string('Tipo', 100);
            $table->string('Motor', 100);
            $table->string('Marca', 100);
            $table->integer('Id_Usuario')->unsigned()->index();
            $table->string('Path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Vehiculos');
    }
}
