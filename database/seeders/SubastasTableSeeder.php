<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Subastas;
use \Datetime;

class SubastasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subastas::create([
            'Puja_Min' => 5,
            'Fecha_Fin' => new DateTime(),
            'Activa' => true,
            'Id_Usuario' => 1,
            'Id_Vehiculo' => 1
        ]);
    }
}
