<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        User::create([
            'Nombre' => 'Oriol',
            'Email' => 'correo@gmail.com',
            'Tipo' => 'Vendedor',
            'Saldo' => 500,
            'password' => Hash::make('super3')
            ]);
    }
}
