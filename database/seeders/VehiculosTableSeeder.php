<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Vehiculos;

class VehiculosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vehiculos::create([
            'Nom' => 'Rayo McQueen',
            'Matricula' => 'M050F',
            'Tipo' => 'Camioneta',
            'Motor' => 'v200',
            'Marca' => 'El Rasho',
            'Id_Usuario' => 1,
            'Path' => 'Jarl'
            ]);
    }
}
