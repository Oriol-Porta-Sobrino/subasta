<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pujas;

class PujasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pujas::create([
            'Precio' => 500,
            'Id_Subasta' => 1,
            'Id_Usuario' => 1
            ]);
    }
}
