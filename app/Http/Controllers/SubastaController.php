<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Vehiculos;
use App\Models\Subastas;
use App\Models\Pujas;
use \Datetime;
use Auth;

class SubastaController extends Controller
{

	public function home() {
        if (Auth::id() == null) {
            return view('auth/login');
        } else {
			$usuario = Auth::user();
            $subastas = Subastas::select('Subastas.Id_Subasta','Subastas.Puja_Min','Subastas.Fecha_Fin','Subastas.Activa','Vehiculos.Matricula','Vehiculos.Marca','Vehiculos.Nom', 'Vehiculos.Path')
            ->join('Vehiculos', 'Subastas.Id_Vehiculo', '=', 'Vehiculos.Id_Vehiculo')
			->where('Activa', '=', '1')
            ->get();

            return view("casaSubastas")->with('subastas', $subastas)->with('usuario', $usuario);
        }
    }

	public function showUser(){
    	$tots=User::all();
    	return $tots->toJson();
	}

	public function auctions(Request $request)
    {
        $id = Auth::id();
        $usuario = User::findOrFail($id);
        if ($usuario['Tipo'] == 'Vendedor') {
        $subastas = Subastas::select('Vehiculos.Path', 'Subastas.Id_Subasta','Subastas.Puja_Min','Subastas.Fecha_Fin','Subastas.Activa','Vehiculos.Matricula','Vehiculos.Marca','Vehiculos.Nom')
                        ->join('Vehiculos', 'Subastas.Id_Vehiculo', '=', 'Vehiculos.Id_Vehiculo')
						->where('Subastas.Id_Usuario', '=', $id)
                        ->get();

            return view("subastas")->with('subastas', $subastas);
        } else {
            return redirect("/")->with("mensaje", "No eres un vendedor");
        }
    }

    public function bids(Request $request)
    {
		$usuario = Auth::user();
		if ($usuario['Tipo'] == 'Comprador') {
			$pujas = Pujas::select('Pujas.Id_Puja', 'Pujas.Precio', 'Vehiculos.Nom', 'Users.Nombre', 'Vehiculos.Path')
                        ->join('Subastas', 'Pujas.Id_Subasta', '=', 'Subastas.Id_Subasta')
						->join('Vehiculos', 'Subastas.Id_Vehiculo', '=', 'Vehiculos.Id_Vehiculo')
						->join('Users', 'Subastas.Id_Usuario', '=', 'Users.id')
						->where('Pujas.Id_Usuario', '=', $usuario->id)
                        ->get();
    		return view("pujas")->with('pujas', $pujas);
		} else {
			return redirect("/")->with("mensaje", "No eres un comprador");
		}
    }

    public function auction(Request $request)
    {
		$id = Auth::id();
		if ($request->subastar == null) {
			$usuario = User::findOrFail($id);
			if ($usuario['Tipo'] == 'Vendedor') {
				$coches = Vehiculos::select('*')->where('Id_Usuario', '=', $id)->get();
				return view("subastar")->with('usuario', $usuario)->with('coches', $coches);
			} else {
				return redirect("/")->with("mensaje", "Solo los vendedores pueden crear subastas");
			}
		} else {
			if ($request->Fecha_Fin > new DateTime()){
				return redirect("/")->with("mensaje", "La fecha fin no puede ser anterior a la actual");
			} else {
				$usuario = User::findOrFail($id);
				$usuario->Saldo = $usuario->Saldo-100;
				$usuario->save();
				$subasta = new Subastas();
				$subasta->Puja_Min = $request->puja_min;
				$subasta->Fecha_Fin = $request->fecha_fin;
				$subasta->Activa = true;
				$subasta->Id_Usuario = $id;
				$subasta->Id_Vehiculo = $request->coche;
				$subasta->save();
				return redirect("/auctions");
			}
		}
    }

    public function puja(Request $request)
    {		
		$usuario = Auth::user();	
		if ($usuario->Tipo == 'Vendedor') {
			return redirect("/")->with("mensaje", "Solo los compradores pueden pujar");
		} else if ($request->comprar == null)	{
			$idSubasta = $request->id;
			$subasta = Subastas::findOrFail($idSubasta);
			
			if ($subasta->Id_Usuario == $usuario->id) {
				return redirect("/")->with("mensaje", "No puedes pujar en tu propia subasta");
			}
			return view("pujar")->with('usuario', $usuario)->with('subasta', $subasta);
		} else {		
			if ($usuario->Saldo >= $request->puja) {
				$idSubasta = $request->id;
				$subasta = Subastas::findOrFail($idSubasta);
				$subasta->Activa = false;
				$usuario->Saldo -= $request->puja;
				$vehiculo = Vehiculos::findOrFail($subasta->Id_Vehiculo);
				$vehiculo->Id_Usuario = $usuario->id;

				$vendedor = User::findOrFail($subasta->Id_Usuario);
				$dinero = $request->puja;
				$dinero -= $dinero*0.03;				
				$vendedor->Saldo += $dinero;
				$vendedor->Saldo += 100;
				$vendedor->save();

				$puja = new Pujas();
				$puja->Id_Usuario = $usuario->id;
				$puja->Precio = $request->puja;
				$puja->Id_Subasta = $idSubasta;
				$puja->save();
				$usuario->save();
				$subasta->save();
				$vehiculo->save();
				return redirect("/")->with("mensaje", "Has hecho la puja correctamente");
			} else {
				return redirect("/")->with("mensaje", "No tienes suficiente dinero");
			}
			
		}
    }

    public function lowerPrice(Request $request)
    {
		$usuario = Auth::user();
		$subasta = Subastas::findOrFail($request->idSubasta);
		if ($subasta->Activa == 1) {
			if ($usuario->id == $subasta->Id_Usuario) {
				//puedes bajar
				$subasta->Puja_Min -= $subasta->Puja_Min*0.05;
				$subasta->save();
				return redirect("/")->with("mensaje", "Has bajado el precio correctamente");
			} else {
				return redirect("/")->with("mensaje", "No puedes bajar el precio a una subasta que no es tuya");
				//no puedes bajar el precio a una subasta que no es tuya
			}
		} else {
			return redirect("/")->with("mensaje", "No puedes bajar el precio a una subasta que esta finalizada");
		}
    }

    public function subastaOut(Request $request)
    {
		$usuario = Auth::user();
		if ($usuario->Tipo == 'Vendedor') {
			$subastas = Subastas::select('*')->where('Id_Usuario', '=', $usuario->id)->get();
			foreach ($subastas as $subasta) {
				if ($subasta->Fecha_Fin > new DateTime()) {
					$subasta->Activa = false;	
					$subasta->save();
				}			
			}
			return redirect("/")->with("mensaje", "Se han actualizado todas tus subastas");
		} else {
			return redirect("/")->with("mensaje", "No eres un vendedor");
		}
    }

    public function getVehiculos(Request $request)
    {
		$usuario = Auth::user();
		$vehiculos = Vehiculos::select('*')->where('Id_Usuario', '=', $usuario->id)->get();
		return view("getVehiculos")->with("vehiculos", $vehiculos);
    }

	public function setVehiculos(Request $request)
    {
		$id = Auth::id();
		if ($request->newitem == null) {
			return view("setcar");
		} else {
			 $request->validate([
                'ruta' => 'required|file|mimes:jpg,jpeg,bmp,png,doc,docx,csv,rtf,xlsx,xls,txt,pdf,zip',
            ]);
			//$carpeta_destino = $_SERVER['DOCUMENT_ROOT'] . "public/img/";
			//move_uploaded_file($request->path, 'img/' . $request->path);
			$ruta = time().'.'.$request->ruta->extension();
			$request->ruta->move(public_path('img'), $ruta);
			$vehiculo = new Vehiculos();
			$vehiculo->Matricula = $request->matricula;
			$vehiculo->Nom = $request->nom;
			$vehiculo->Tipo = $request->tipo;
			$vehiculo->Motor = $request->motor;
			$vehiculo->Path = $ruta;
			$vehiculo->Marca = $request->marca;
			$vehiculo->Matricula = $request->matricula;
			$vehiculo->Id_Usuario = $id;
			$vehiculo->save();
			return redirect("/myObjects");
		}
    }
}
