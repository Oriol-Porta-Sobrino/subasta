<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Nombre', 'Email', 'Tipo', 'Saldo', 'password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'Email_Verified_At' => 'Datetime',
    ];

    public function Usuarios()
    {
        return $this->hasMany(Pujas::class);
    }

    public function Vehiculos()
    {
        return $this->hasMany(Vechiculos::class);
    }
    
    public function Subastas()
    {
        return $this->hasMany(Subastas::class);
    }
}
