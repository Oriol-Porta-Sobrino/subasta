<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subastas extends Model
{
    use HasFactory;
    protected $primaryKey = 'Id_Subasta';
    protected $fillable = ['Id_Subasta', 'Puja_Min', 'Fecha_Fin', 'Activa', 'Id_Usuario', 'Id_Vehiculo'];

    public function Usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function Vehiculos()
    {
        return $this->belongsTo(Vehiculos::class, 'Id_Vehiculo');
    }

    public function Puja()
    {
        return $this->hasOne(Pujas::class);
    }
}
