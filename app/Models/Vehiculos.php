<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehiculos extends Model
{
    use HasFactory;
    protected $primaryKey = 'Id_Vehiculo';
    protected $fillable = ['Id_Vehiculo', 'Nom', 'Matricula', 'Tipo', 'Motor', 'Path', 'Marca', 'Id_Usuario'];

    public function Usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function Subastas()
    {
        return $this->hasMany(Subastas::class);
    }
}
