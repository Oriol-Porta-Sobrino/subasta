<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pujas extends Model
{
    use HasFactory;
    protected $primaryKey = 'Id_Puja';
    protected $fillable = ['Id_Puja', 'Precio', 'Id_Subasta', 'Id_Usuario'];

    public function Usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function Subasta()
    {
        return $this->belongsTo(Subastas::class, 'Id_Subasta');
    }
}
