<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Oriol Porta Sobrino">
    <meta name="author" content="Isaac García Jiménez">
    <meta name="description" content="Nuevo Item">
    <title>Nuevo Item</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>ERROR!</strong> Eres un pleb.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
    <div class="col-md-10 offset-sm-1">
        <h1 class="display-3 text-center">Nuevo Vehiculo</h1>
        <div class="card p-3 mb-2 bg-light text-dark">
            <div class="card-body">
                <a href="../" class="btn btn-danger">Home</a><br><br>
                <form method="POST" enctype="multipart/form-data" action="/setItem">
                    <div class="form-group row">
                        <label for="matricula" class="col-sm-2 col-form-label">Matricula</label>
                        <div class="col-sm-10">
                            <input type="text" maxlength="20" class="form-control" id="matricula" name="matricula" placeholder="0000AAA" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="marca" class="col-sm-2 col-form-label">Marca</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="marca" name="marca" placeholder="Seat" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nom" class="col-sm-2 col-form-label">Modelo</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nom" name="nom" placeholder="Ibiza" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="motor" class="col-sm-2 col-form-label">Motor</label>
                        <div class="col-sm-10">
                        <select name="motor" id="motor">
                                <option>Diesel</option>
                                <option>Gasolina</option>
                                <option>Hibrido</option>
                                <option>Eléctrico</option>
                                <option>Gas</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tipo" class="col-sm-2 col-form-label">Tipo</label>
                        <div class="col-sm-10">
                            <select name="tipo" id="tipo">
                                <option>Turismo</option>
                                <option>Mixto</option>
                                <option>Furgoneta</option>
                                <option>Camión</option>
                                <option>Motocicleta</option>
                            </select>
                        </div>
                    </div>
                    @csrf
                    <div class="form-group row">
                        <label for="ruta" class="col-sm-2 col-form-label">Imagén</label>
                        <div class="col-sm-10">
                            <input type="file"  class="form-control" id="ruta" name="ruta">
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-sm-8 offset-sm-2 text-center">
                            <input type="submit" class="btn btn-primary" id="newitem" name="newitem" value="Crear Vehiculo"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>