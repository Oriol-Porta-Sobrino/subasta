<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Oriol Porta Sobrino">
    <meta name="author" content="Isaac García Jiménez">
    <meta name="description" content="Subastar item">
    <title>Subastar</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="col-md-10 offset-sm-1">
        <h1 class="display-3 text-center">Compra el coche al mejor precio</h1>
        <div class="card p-3 mb-2 bg-light text-dark">
            <div class="card-body">
            <a href="../" class="btn btn-danger">Home</a><br><br>
                <form>
                    <div class="form-group row">
                        <label></label>
                        <label for="puja_min" class="col-sm-2 col-form-label">El precio es {{ $subasta['Puja_Min'] }}</label>
                        <div class="col-sm-10">
                        <input type="number"  class="form-control" id="puja" name="puja" min="{{ $subasta['Puja_Min'] }}" max="{{ $subasta['Puja_Min'] }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 offset-sm-2 text-center">
                            <input type="submit" class="btn btn-primary" id="comprar" name="comprar" value="Comprar">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>