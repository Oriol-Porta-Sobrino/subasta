<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Oriol Porta Sobrino">
    <meta name="author" content="Isaac García Jiménez">
    <meta name="description" content="Subastar item">
    <title>Subastar</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="col-md-10 offset-sm-1">
        <h1 class="display-3 text-center">Crear Subasta</h1>
        <div class="card p-3 mb-2 bg-light text-dark">
            <div class="card-body">
                <a href="../" class="btn btn-danger">Home</a><br><br>
                <form>
                    <div class="form-group row">
                        <label for="puja_min" class="col-sm-2 col-form-label">Puja minima</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="puja_min" name="puja_min" placeholder="€" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="fecha_fin" class="col-sm-2 col-form-label">Fecha fin</label>
                        <div class="col-sm-10">
                            <input type="date"  class="form-control" id="fecha_fin" name="fecha_fin" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Vehiculo a subastar</label>
                        <div class="col-sm-10">
                            <select name="coche" id="coche" required>
                                @foreach ($coches as $coche)
                                    <option value="{{ $coche['Id_Vehiculo'] }}">{{ $coche['Matricula'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-8 offset-sm-2 text-center">
                            <button type="submit" class="btn btn-primary" id="subastar" name="subastar" value="Crear subasta">Crear Subasta</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>