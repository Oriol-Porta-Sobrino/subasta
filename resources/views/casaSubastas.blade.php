<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Oriol Porta Sobrino">
    <meta name="author" content="Isaac García Jiménez">
    <meta name="description" content="Subastar item">
    <title>Casas de subastas Oriol & sAc</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    @if ($message = Session::get('mensaje'))
    <script>
            alert('{{$message}}');
    </script>
    @endif
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif
                        
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    <div class="col-md-10 offset-sm-1">
        <h1 class="display-3 text-center">Subastas Oriol & sAc - Subastas</h1>
        <div class="card p-3 mb-2 bg-light text-dark">
            <div class="card-body">
                <div class="card-body">
                    <a href="myObjects" class="btn btn-outline-primary">Ver coches</a>
                    <a href="check" class="btn btn-outline-primary">Comprobar subastas</a>
                    <a href="auction" class="btn btn-outline-primary">Crear subasta</a>
                    <a href="auctions" class="btn btn-outline-primary">Ver mis subastas</a>
                    <a href="bids" class="btn btn-outline-primary">Ver mis pujas</a>
                <br><br>
                <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                    <th>Id Subasta</th>
                    <th>Puja Minima</th>
                    <th>Fecha finalización</th>
                    <th>Activa</th>
                    <th>Matricula</th>
                    <th>Marca</th>
                    <th>Nom</th>
                    <th>Foto</th>
                    <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($subastas as $subasta)
                        <tr>
                            <th scope="row">{{ $subasta['Id_Subasta'] }} </th>
                            <td>{{ $subasta['Puja_Min'] }}</td>
                            <td>{{ $subasta['Fecha_Fin'] }}</td>
                            <td>{{ $subasta['Activa'] }}</td>
                            <td>{{ $subasta['Matricula'] }}</td>
                            <td>{{ $subasta['Marca'] }}</td>
                            <td>{{ $subasta['Nom'] }}</td>
                            <td><a href="img/{{ $subasta['Path'] }}" download><img class="img-thumbnail" src="img/{{ $subasta['Path'] }}" alt="Generic placeholder image" width="64px" high="64px"></a></td>
                            <td>
                                <a href="bid/{{$subasta['Id_Subasta']}}" class="btn btn-primary">Pujar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
</body>
</html>