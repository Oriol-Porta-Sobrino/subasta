<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Oriol Porta Sobrino">
    <meta name="author" content="Isaac García Jiménez">
    <meta name="description" content="Subastar item">
    <title>Coches</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="col-md-10 offset-sm-1">
        <h1 class="display-3 text-center">Subastas Oriol & sAc - Subastas</h1>
        <div class="card p-3 mb-2 bg-light text-dark">
            <div class="card-body">
                <a href="/" class="btn btn-danger">Home</a>
                <a href="newItem" class="btn btn-outline-primary">Nuevo Vehículo</a>
            <br><br>
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th>Matricula</th>
                        <th>Marca</th>
                        <th>Modelo</th>
                        <th>Motor</th>
                        <th>Tipo</th>
                        <th>Foto</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($vehiculos as $vehiculo)
                        <tr>
                            <td>{{ $vehiculo['Matricula'] }}</td>
                            <td>{{ $vehiculo['Marca'] }} </td>
                            <td>{{ $vehiculo['Nom'] }}</td>
                            <td>{{ $vehiculo['Motor'] }}</td>
                            <td>{{ $vehiculo['Tipo'] }}</td>
                            @if ($vehiculo['Path'] != null)
                            <td><a href="img/{{ $vehiculo['Path'] }}" download><img class="img-thumbnail" src="img/{{ $vehiculo['Path'] }}" alt="Car image" width="64px" high="64px"></a></td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>